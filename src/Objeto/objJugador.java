/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objeto;

import java.util.ArrayList;

/**
 *
 * @author Marlon T
 */
public class objJugador {
    
    private String nombre;
    private String fecha;
    private String movimientosAdyasantes;
    private String saltos;
    private String tipo;
   
    
    public objJugador(String nombre, String fecha, String movimientosAdyasantes, String saltos, String tipo) {
        
        this.nombre = nombre;
        this.fecha = fecha;
        this.movimientosAdyasantes = movimientosAdyasantes;
        this.saltos = saltos;
        this.tipo = tipo;
        
    }
    
    public static ArrayList listaJugadores = new ArrayList<>();
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMovimientosAdyasantes() {
        return movimientosAdyasantes;
    }

    public void setMovimientosAdyasantes(String movimientosAdyasantes) {
        this.movimientosAdyasantes = movimientosAdyasantes;
    }

    public String getSaltos() {
        return saltos;
    }

    public void setSaltos(String saltos) {
        this.saltos = saltos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

       
}
