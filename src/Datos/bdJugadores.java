/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Objeto.objJugador;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Marlon T
 */
public class bdJugadores {
    
    public void InsertarEnArchivo(String datosJugador) {
        try {
            File archivo = new File("Lista_Jugadores.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datosJugador + "\r\n");
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    public ArrayList<String> LeerDesdeArchivo() {
        ArrayList<String> listaJugadores = new ArrayList<>();
        try {
            File archivo = new File("Lista_Jugadores.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                listaJugadores.add(archi.readLine());
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return listaJugadores;
    }
    
    
    public ArrayList<objJugador> LeerObjetoJugadores(){
        try {
            File archivo = new File("Lista_Jugadores.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                String[] separar = new String[5];
                separar = archi.readLine().split(",");
                objJugador.listaJugadores.add(new objJugador (separar[0],separar[1],separar[2],separar[3],separar[4]));
            }
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer del archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
        return objJugador.listaJugadores;
    }
    
}
